package com.example.intentservices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SecondService extends Service {
    String TAG = "MainActivity";
    public SecondService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int maxCount = 100;
        final int sleepTime = 1000;
        Runnable runnable = new Runnable() {			// Creación de un Objeto de tipo Runnable
            @Override
            public void run() {						// Implementación de la Lógica
                for (int i = 1; i <= maxCount; i++) {
                    Log.i(TAG, "onStartCommand - count: " + i);
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {e.printStackTrace();}
                }
            }
        };

        Thread thread = new Thread(runnable, "Hilo Segundo Servicio");		// Instancia de Thread
        thread.start(); 										// Inicio de ejecución
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
