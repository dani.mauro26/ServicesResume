package com.example.intentservices;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;


public class FourthService extends IntentService {
    public static final String PARAM_MAX_COUNT = "param_max_count";
    public static final String PARAM_SLEEP_TIME = "param_sleep_time";
    private String TAG = "CuartoService";

    public FourthService(String name, String TAG) {
        super(name);
        this.TAG = TAG;
    }

    public FourthService() {
        super("FourthService");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int maxCount = intent.getIntExtra(PARAM_MAX_COUNT, 100);
        int sleepTime = intent.getIntExtra(PARAM_SLEEP_TIME, 200);

        for (int i = 1; i <= maxCount; i++) {
            Log.i(TAG, "onStartCommand - count: " + i);
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

/*
* 1. Después de que tiempo significativo haya transcurrido, la app no manda: appNotRespondig
* 2. Los recursos se liberan completamente después de la ejecución.*/