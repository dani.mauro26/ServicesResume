package com.example.intentservices;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Random;

public class BindServices extends Service {
    public BindServices() {
    }

    private LocalBinder binder = new LocalBinder();
    private Random random = new Random();

    public class LocalBinder extends Binder{
        public BindServices getService(){
            return BindServices.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    public int getRandomNumber(int maximoValor){
        return random.nextInt(maximoValor);
    }
}
