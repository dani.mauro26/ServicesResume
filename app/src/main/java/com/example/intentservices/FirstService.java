package com.example.intentservices;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class FirstService extends Service {
    public FirstService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int maxCount = 100;
        int sleepTime = 2000;
        for (int i = 1; i <= maxCount; i++) {
            System.out.println("onStartCommand - count: " + i);
            try {
                Thread.sleep(sleepTime); // Detiene el proceso X milisegundos
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

/*
* 1. Se puede ejecutar otra aplicación, pero no abre hasta que el hilo principal termine su proceso
* 2. El hilo principal se bloquea y la app manda: appNoResponding 20 secs after.
* 3. Dichos valores se pueden enviar desde la función Intent.putExtra()*/
