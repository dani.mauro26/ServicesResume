package com.example.intentservices;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

public class ThirdService extends Service {
    private static final String PRINCIAL_CHANNEL_ID = "1";

    public ThirdService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel =
                    new NotificationChannel(PRINCIAL_CHANNEL_ID, "Canal principal", importance);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int maxCount = 100;
        final int sleepTime = 50;

        final NotificationCompat.Builder notificationBuilder;
        notificationBuilder = new NotificationCompat.Builder(this, PRINCIAL_CHANNEL_ID);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Notificación del tercer servicio");

        for (int i = 1; i <= maxCount; i++) {
            try {
                Thread.sleep(sleepTime);
                System.out.println("onStartCommand - count: " + i);
                notificationBuilder.setProgress(100,i,false);
                Notification processNotification = notificationBuilder.build();
                startForeground(200, processNotification);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        stopForeground(true);
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

/*
* Respuestas de la actividad:
* 1. Aparece una notificación con asunto: setContentTittle
* 2. Con el método stopForeground(true) desaparece la notificación, se desvincula la notificación a la ejecución del servicio
* 3. Con el comando setProgress en el setNotificationBuilder se asigna el progreso antes de tener ejecutar la notificación
* 4. Los recursos no son liberados después de ejecutar la lógica dentro del onStartCommand
* 5. Se liberan los recursos de la memoria con el comando stopSelf()
* NOTA: Las notificaciones deben ejecutarse en el hilo principal.*/
