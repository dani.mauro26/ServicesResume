package com.example.intentservices.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.intentservices.BindServices;
import com.example.intentservices.R;

public class BindServicesActivity extends AppCompatActivity implements View.OnClickListener {

    EditText maximoValorET;
    TextView numeroAleatorioTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_services2);
        maximoValorET = findViewById(R.id.maximoValorET);
        numeroAleatorioTV = findViewById(R.id.numeroAleatorioTV);
    }

    String TAG = "fifthServiceActivity";

    private BindServices bindServices;
    private boolean aBoolean = false;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "onServiceConnected()");
            BindServices.LocalBinder binder = (BindServices.LocalBinder) service;
            bindServices = binder.getService();
            aBoolean = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            aBoolean = false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, BindServices.class);
        bindService(intent, connection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onServiceDisconnected()");
        System.out.println("OnServiceDisconnected()");
        if (aBoolean){
            unbindService(connection);
            aBoolean = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnOn:
                onStart();
                Toast.makeText(bindServices, "ServicioIniciado", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnOff:
                onStop();
                Toast.makeText(bindServices, "ServicioDetenido", Toast.LENGTH_SHORT).show();
                break;

            case R.id.generateRandomInt:
                if(aBoolean){
                    int maximoValor;
                    int numeroAleatorio;

                    if (!maximoValorET.getText().toString().isEmpty()){
                        maximoValor = Integer.valueOf(maximoValorET.getText().toString());
                    } else {
                        maximoValor = 100;
                    }

                    numeroAleatorio = bindServices.getRandomNumber(maximoValor);
                    numeroAleatorioTV.setText(Integer.toString(numeroAleatorio));
                }
                break;

        }
    }
}
