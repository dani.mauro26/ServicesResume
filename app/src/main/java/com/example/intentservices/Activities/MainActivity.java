package com.example.intentservices.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.content.ClipData;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.intentservices.FifthService;
import com.example.intentservices.FirstService;
import com.example.intentservices.FourthService;
import com.example.intentservices.R;
import com.example.intentservices.SecondService;
import com.example.intentservices.ThirdService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";
    private boolean mBool = false;
    private Intent firstServiceIntent, secondServiceIntent, thirdServiceIntent, fourthServiceIntent, fifthServiceIntent, bindedActivityIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstServiceIntent = new Intent(this, FirstService.class);
        secondServiceIntent = new Intent(this, SecondService.class);
        thirdServiceIntent = new Intent(this, ThirdService.class);
        fourthServiceIntent = new Intent(this, FourthService.class);
        fifthServiceIntent = new Intent(this, FifthService.class);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.startServicebtn:
                onStart();
                startService(firstServiceIntent);
                break;
            case R.id.stopServicebtn:
                onStop();
                startService(secondServiceIntent);
                break;
            case R.id.generateRandomIntButton:
                startService(thirdServiceIntent);
                break;
            case R.id.CuartoServicioBtn:
                startService(fourthServiceIntent);
                break;
            case R.id.QuintoServicioBtn:
                startService(fifthServiceIntent);
                break;
            case R.id.BindedServiceBtn:
                bindedActivityIntent = new Intent(this, BindServicesActivity.class);
                startActivity(bindedActivityIntent);
                break;
        }
    }
}

