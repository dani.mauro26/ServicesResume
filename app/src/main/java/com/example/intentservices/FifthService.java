package com.example.intentservices;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class FifthService extends IntentService {

    private static final String PARAM_SLEEP_TIME = "";
    private static final String PARAM_MAX_COUNT = "";
    private static final String PRINCIAL_CHANNEL_ID = "";
    private static final String TAG = "FifthService";

    public FifthService() {
        super("FifthService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel =
                    new NotificationChannel(PRINCIAL_CHANNEL_ID, "Canal principal", importance);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int maxCount = intent.getIntExtra(PARAM_MAX_COUNT, 100);
        int sleepTime = intent.getIntExtra(PARAM_SLEEP_TIME, 100);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, PRINCIAL_CHANNEL_ID);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Notificación del quinto servicio");

        for (int i = 1; i <= maxCount; i++) {
            Log.i(TAG, "onStartCommand - count: " + i);
            notificationBuilder.setProgress(maxCount,i,false);
            startForeground(200, notificationBuilder.build());

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {e.printStackTrace();}
        }

    }
}

/*
* 1. Este tipo de servicio, es el mismo que el cuarto, solo que con el complemento de las notificaciónes.
* Concluyo que resulta ser el mas efectivo y el que pienso aplicar a la hora de desarrollar la app del reproductor
* */
